const { authJwt } = require("../middlewares");
const controller = require("../controllers/admin.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  app.get(
    "/api/admin/getAllUsers",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getAllUsers
  );



  app.post(
    "/api/admin/banUser",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.banUser
  );
  
  app.post(
    "/api/admin/updateUserRole",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.updateUserRole
  );


};
