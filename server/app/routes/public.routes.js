const { authJwt } = require("../middlewares");
const controller = require("../controllers/public.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/public/medicines",
    controller.getAllMedicines
  );

  app.post(
    "/api/public/reportUser",
    controller.reportUser
  );

  app.post(
    "/api/public/reportMed",
    controller.reportMed
  );


  
};
