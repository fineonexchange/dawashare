const { authJwt } = require("../middlewares");
const controller = require("../controllers/mod.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  app.get(
    "/api/mod/medicines",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.getAllMedicines
  );

  app.get(
    "/api/mod/getUsers",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.getUsers
  );


  app.post(
    "/api/mod/updateMedicine",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.updateMedicine
  );
  

  app.post(
    "/api/mod/acceptMedicine",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.acceptMedicine
  );

  app.post(
    "/api/mod/rejectMedicine",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.rejectMedicine
  );


  app.post(
    "/api/mod/banUser",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.banUser
  );

  
  app.get(
    "/api/mod/reports",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.getReports
  );
  
  app.delete(
    "/api/mod/delete/:id",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.deleteMedicine
  );

  app.post(
    "/api/mod/updateReport",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.updateReport
  );

  app.post(
    "/api/mod/assignToMe",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.assignToMe
  );
  

};
