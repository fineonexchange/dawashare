const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.medicine = require('./medicine.model');
db.report = require('./report.model');

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;