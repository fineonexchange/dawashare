const mongoose = require("mongoose");

const Report = mongoose.model(
  "Report",
  new mongoose.Schema({
    
    text: String,
    target:String, //'user', 'med'
    date: Date,
    isResolved:Boolean,
    medId:  mongoose.Schema.Types.ObjectId,
    userId:  mongoose.Schema.Types.ObjectId,
    modId: mongoose.Schema.Types.ObjectId
  })
);

module.exports = Report;