const mongoose = require("mongoose");

const Medicine = mongoose.model(
  "Medicine",
  new mongoose.Schema({
    name: String,
    phone: String,
    area: String,
    dateOfCreation: {
      type: Date,
      default: Date.now
    },
    dateOfAcceptance: Date,
    dateOfRejection: Date,
    reasonOfRejection: String,
    status: {
      type: String,
      enum: ["pending", "accepted", "rejected"]
    },
    userId: mongoose.Schema.Types.ObjectId,
  })
);

module.exports = Medicine;