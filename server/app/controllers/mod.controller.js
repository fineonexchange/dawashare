

const db = require("../models");
const Medicine = db.medicine;
const Report = db.report;
const User = db.user;
const Role = db.role;
const mongoose = require('mongoose');
const moment = require('moment');
const emailUtils = require('../helpers/emailHelpers');

exports.getAllMedicines = async (req, res) => {
  try {
    const aggregate = await Medicine.aggregate([
      {
        $lookup: {
          from: 'users',
          localField: 'userId',
          foreignField: '_id',
          as: 'userData'
        }
      },
      {
        $unwind: "$userData",
      },
      {
        $sort: {"dateOfCreation": -1}
      }
    ]);

    aggregate.forEach(e => {
      e.username = e.userData.username;
    })
    res.status(200).json({ message: 'success', data: aggregate });
  }
  catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}

exports.getUsers = async (req, res) => {
  try {
    let role = await Role.findOne({name:'user'}).lean();
    let roleId = role._id;
    let allUsers = await User.find({roles: roleId}).lean();

    res.status(200).json({ message: 'success', data: allUsers });
  }
  catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}

exports.banUser = async (req, res) => {
  try {
    if (!req.body.id) {
      return res.status(500).send({ message: "missing info" });
    }
    let id = mongoose.Types.ObjectId(req.body.id);
    let targetUser = await User.findOne({_id: id});
    targetUser.isBanned = req.body.isBanned;
    dateOfBan = req.body.isBanned? new Date(): null;
    targetUser.save();
    res.status(200).json({ message: 'success' });
  }
  catch (e) {f
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}


exports.getReports = async (req, res) => {
  try {
    const aggregate = await Report.aggregate([
      {
        $lookup: {
          from: 'users',
          localField: 'userId',
          foreignField: '_id',
          as: 'userData'
        }
      },
      {
        $unwind: "$userData",
      },
      {
        $lookup: {
          from: 'users',
          localField: 'modId',
          foreignField: '_id',
          as: 'modData'
        }
      },
     
      {
        $lookup: {
          from: 'medicines',
          localField: 'medId',
          foreignField: '_id',
          as: 'medData'
        }
      },
      {
        $unwind: "$medData",
      },


    ]);

    aggregate.forEach(e => {
      e.username = e.userData.username;
      e.medName = e.medData.name;
      e.modName = e.modData[0] ? e.modData[0].username : 'None';
      e.formattedDate = moment(e.date).format('MM/DD/YY HH:mm:ss')
      delete e.modData;
      delete e.userData;
      delete e.medData;
      if(e.modId){
        if(e.modId+"" !== req.userId+""){
          e.isAssignedToSomeoneElse = true;
        }
        if(e.modId+"" === req.userId+""){
          e.isAssignedToMe = true;
        }
        
      }
   
    })
    res.status(200).json({ message: 'success', data: aggregate });
  }
  catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}

exports.deleteMedicine = async (req, res) => {
  try {

    if (!req.params.id) {
      return res.status(500).send({ message: "missing info" });
    }

    let id = mongoose.Types.ObjectId(req.params.id);

    await Medicine.deleteOne({ _id: id });

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}

exports.updateReport = async (req, res) => {
  try {
    if (!req.body.id) {
      return res.status(500).send({ message: "missing info" });
    }

    let id = mongoose.Types.ObjectId(req.body.id);
    let target = await Report.findOne({ _id: id });

    Object.keys(req.body).forEach(prop => {
      if(prop !== 'id'){
        target[prop] = req.body[prop]
      }
    })

    target.save();

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });

  }
}

exports.assignToMe = async (req, res) => {
  try {
    if (!req.body.id) {
      return res.status(500).send({ message: "missing info" });
    }

    let id = mongoose.Types.ObjectId(req.body.id);
    let target = await Report.findOne({ _id: id });

    target.modId = req.body.isAssigned ? req.userId : null;

    target.save();

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });

  }
}

exports.updateMedicine = async (req, res) => {
  try {
    if (!req.body.id) {
      return res.status(500).send({ message: "missing info" });
    }

    let id = mongoose.Types.ObjectId(req.body.id);
    let target = await Medicine.findOne({ _id: id });

    Object.keys(req.body.formData).forEach(prop => {
      target[prop] = req.body.formData[prop]
    })

    target.save();

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}


exports.acceptMedicine = async (req, res) => {
  try {
    if (!req.body.id) {
      return res.status(500).send({ message: "missing info" });
    }

    let id = mongoose.Types.ObjectId(req.body.id);
    let target = await Medicine.findOne({ _id: id });

    target.status = 'accepted';
    target.dateOfAcceptance = new Date();

    target.save();

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}


exports.rejectMedicine = async (req, res) => {
  try {
    if (!req.body.id || !req.body.formData) {
      return res.status(500).send({ message: "missing info" });
    }
     
    const id = mongoose.Types.ObjectId(req.body.id);
    const targetMed = await Medicine.findOne({ _id: id });


    targetMed.status = 'rejected';
    targetMed.dateOfRejection = new Date();
    targetMed.reasonOfRejection = req.body.formData;
    const targetUser = await User.findOne({_id: targetMed.userId}).lean();
    const textBody =  `Your medicine record has been rejected on DawaShare, reason of rejection: <br> 
    ${req.body.formData}
    `
    await emailUtils.sendEmail(targetUser.email,'Your medicine record got rejected',textBody, 'User')
    targetMed.save();

    res.status(200).json({ message: 'success' });
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'error' });
  }
}