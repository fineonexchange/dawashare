const db = require("../models");
const Medicine = db.medicine;
const Report = db.report;
const User = db.user;
const Role = db.role;
const mongoose = require('mongoose');
const moment = require('moment');


exports.getAllUsers = async (req, res) => {
    try {
        let allUsers = await User.find({}).lean();
        let allRoles = await Role.find({}).lean();

        allUsers.forEach(user => {
            user.roleNames = [];
            user.roles.forEach(roleId => {
            let foundRole = allRoles.find(e => e._id+"" === roleId+"" );
            if(foundRole){
                user.roleNames.push({roleId: foundRole._id, roleName: foundRole.name});

                if(foundRole.name === 'moderator'){
                    user.isMod = true;
                }
                if(foundRole.name === 'admin'){
                    user.isAdmin = true;
                }
                if(foundRole.name === 'user'){
                    user.isUser = true;
                }
            }
            })
        })

        res.status(200).json({ message: 'success', data: allUsers });
    }
    catch (e) {
        console.log(e)
        res.status(500).json({ message: 'error' });
    }
}


exports.updateUserRole = async (req, res) => {
    try {
        if (!req.body.id || !req.body.formData) {
            return res.status(500).send({ message: "missing info" });
        }
        let id = mongoose.Types.ObjectId(req.body.id);
        let allRoles = await Role.find({}).lean();


        let targetUser = await User.findOne({_id: id});
        targetUser.roles = [];

        if(req.body.formData.isAdmin){
            targetUser.roles.push( allRoles.find(e=> e.name === 'admin'));
        }
        if(req.body.formData.isMod){
            targetUser.roles.push( allRoles.find(e=> e.name === 'moderator'));
        }
        if(req.body.formData.isUser){
            targetUser.roles.push( allRoles.find(e=> e.name === 'user'));
        }
     

        targetUser.save();

        res.status(200).json({ message: 'success' });
    }
    catch (e) {
        console.log(e)
        res.status(500).json({ message: 'error' });
    }
}

exports.banUser = async (req, res) => {
    try {
      if (!req.body.id) {
        return res.status(500).send({ message: "missing info" });
      }
      let id = mongoose.Types.ObjectId(req.body.id);
      let targetUser = await User.findOne({_id: id});
      targetUser.isBanned = req.body.isBanned;
      dateOfBan = req.body.isBanned? new Date(): null;
      targetUser.save();
      res.status(200).json({ message: 'success' });
    }
    catch (e) {f
      console.log(e)
      res.status(500).json({ message: 'error' });
    }
  }
  

