

const db = require("../models");
const Medicine = db.medicine;
const mongoose = require('mongoose');

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("Your medicines");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};


exports.addMedicine = (req, res) => {
  try{
  let formData = req.body.formData;
  console.log(formData)
  if(!formData.name || !formData.area || !formData.phone) {
    return res.status(500).send({ message: "missing info" });
  }

  let newEntry = {name: formData.name, area: formData.area, phone: formData.phone , status: 'pending'};
  newEntry.userId = new mongoose.Types.ObjectId(req.userId);

 Medicine.create(newEntry);
 res.status(200).json({message:'success'});
}catch(e){
  console.log(e)
  res.status(500).json({message:'error'});
}
}

exports.updateMedicine = async (req, res) => {
  try{
  let formData = req.body.formData;
  if( !req.body.id) {
   return  res.status(500).send({ message: "missing info" });
  }

  let id = mongoose.Types.ObjectId(req.body.id);
 let target = await Medicine.findOne({_id:id});

 if(target.userId.toString() != req.userId.toString()){
  res.status(500).send({ message: "not allowed" });
}

 Object.keys(formData).forEach(field => {
   target[field] = formData[field]
 })

target.save();
res.status(200).json({message:'success'});


}catch(e){
  console.log(e)
  res.status(500).json({message:'error'});
}
}



exports.deleteMedicine = async (req, res) => {
  try{

    if(!req.params.id || req.params.id === 'undefined') {
      res.status(500).send({ message: "missing info" });
    }


console.log(req.params.id)
let id = mongoose.Types.ObjectId(req.params.id);

let target = await Medicine.findOne({_id:id}).lean();

if(target.userId.toString() != req.userId.toString()){
  res.status(500).send({ message: "not allowed" });
}

 await Medicine.deleteOne({_id:id});

 res.status(200).json({message:'success'});
}catch(e){
  console.log(e)
  res.status(500).json({message:'error'});
}
}


exports.getMyMedicines = async (req, res) => {
  try{
  let id = new mongoose.Types.ObjectId(req.userId);
  let medicines = await Medicine.find({userId:id}).lean();

 res.status(200).json({message:'success', data: medicines});
}
catch(e){
  res.status(500).json({message:'error'});
}
}

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};
