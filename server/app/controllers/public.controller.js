
const db = require("../models");
const Medicine = db.medicine;
const User = db.user;
const Role = db.role;
const Report = db.report;
const emailUtils = require('../helpers/emailHelpers');

const mongoose = require('mongoose');

exports.getAllMedicines = async (req, res) => {
    try{
    let allMedicines = await Medicine.find({status:'accepted'}).lean();
   res.status(200).json({message:'success', data: allMedicines});
  }
  catch(e){
    console.log(e)
    res.status(500).json({message:'error'});
  }
  }


  exports.reportUser = async (req, res) => {
    try{

    if(!req.body.text) {
      return res.status(500).send({ message: "missing info" });
    }

    let record = {target: 'user', date: new Date(), text: req.body.text, medId:req.body.medId, userId:req.body.userId}
    await Report.create(record);

    let modRole = await Role.findOne({name:'moderator'}).lean();
    let modRoleId = modRole._id;
    let allMods = await User.find({roles: modRoleId}).lean();

    let textBody = `A user has been reported by a visitor, please check your account. <br>
    Please note that the other mods will receive the same email, so coordinate with them. <br>
    the reason of report: ${req.body.text}`

    for (let index = 0; index < allMods.length; index++) {
      const mod = allMods[index];
      await emailUtils.sendEmail(mod.email,'A user got reported',textBody, 'Mod')
    }

   res.status(200).json({message:'success'});
  }
  catch(e){
    console.log(e)
    res.status(200).json({message:'error'});
  }
  }

  exports.reportMed = async (req, res) => {
    try{

    if(!req.body.text) {
      return res.status(500).send({ message: "missing info" });
    }

    let record = {target: 'med', date: new Date(), text: req.body.text, medId:req.body.medId, userId:req.body.userId}
    await Report.create(record);

    let modRole = await Role.findOne({name:'moderator'}).lean();
    let modRoleId = modRole._id;
    let allMods = await User.find({roles: modRoleId}).lean();

    let textBody = `A record has been reported by a visitor, please check your account. <br>
    Please note that the other mods will receive the same email, so coordinate with them. <br>
    the reason of report: ${req.body.text}`

    for (let index = 0; index < allMods.length; index++) {
      const mod = allMods[index];
      await emailUtils.sendEmail(mod.email,'A record got reported',textBody, 'Mod')
    }


   res.status(200).json({message:'success'});
  }
  catch(e){
    console.log(e)
    res.status(500).json({message:'error'});
  }
  }