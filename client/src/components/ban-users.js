import React, { Component } from "react";

import ModService from "../services/mod.service";
import EventBus from "../common/EventBus";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import Input from "react-validation/build/input";
 
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";


/* const columns = [
  { key: "_id", name: "id", width: "20%", hidden: false, resizable: true },
  { key: "username", name: "User", width: "10%", resizable: true },

  { key: "phone", name: "Phone/Mobile Number", editable: true, width: "40%", resizable: true },
  { key: "area", name: "Area", editable: true, width: "10%", resizable: true },

  { key: "action", name: "Actions", resizable: true, width: "10%" }
]; */



const rows = [];

var originalRows = [];



export default class BanUsers extends Component {

  constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
    this.cancelSearch = this.cancelSearch.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.columns = [
    
      {
        Header: "Username",
        accessor: "username",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Email",
        accessor: "email",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Is Banned",
        accessor: "isBanned",
        show: false
      }
      ,{
        id: 'edit',
        Header: "Actions",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' },
        accessor: '_id',
        // Set the custom cell render here:
        Cell: (cell) => (
          <div style={{ 'textAlign': "left" }}>
           <input type="checkbox" defaultChecked={cell.row.isBanned} onChange={() => {this.handleBan(cell)}} />&nbsp;
           Banned 
          </div>
        )
      },



    ];

    this.state = {
      content: "",
      rows: rows,
      editMode: 'No',
      originalRows: originalRows,
    };
  }

  
  getUsers() {
    ModService.getUsers().then(resp => {
      let originalRows = [...resp.data.data];
      
      this.setState({ rows:  [...originalRows], originalRows: originalRows, loading: false });
    }, error => {
      this.setState({
        content:
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString()
      });

      if (error.response && error.response.status === 401) {
        EventBus.dispatch("logout");
      }
    }
    )
  }


  componentDidMount() {
    this.getUsers();
  }


 
  handleBan =  async (cell) => {
    cell.row.isBanned = !cell.row.isBanned;
    await ModService.banUser(cell.original._id, cell.row.isBanned);
    await this.getUsers();

  }


  handleResolve = (cell) => {
    let rowId = cell.value;
    let row = this.state.rows.find(e => e._id === rowId);
    let index = this.state.rows.findIndex(e => e._id === rowId);

    let rows = [ ...this.state.rows ];
    rows[index] = {...rows[index], isResolved: true};
    this.setState({ rows });

    if (row) {
      this.setState(previousState => ({
        editMode: 'Yes',
      }), () => {
        this.setState({ editMode: 'Yes', name: row.name, phone: row.phone, area: row.area, id: row._id });
      });
    }

  }

  onSearch = () => {
    let toSearch = this.state.searchVal ? this.state.searchVal.toLowerCase() : null;
    originalRows = this.state.originalRows;
    this.setState({ loading: true, searching: true });
    let results = [];
    let finalResults = [];
    for (var i = 0; i < originalRows.length; i++) {
      for (let key in originalRows[i]) {
        if (key === '_id' || key ==='isResolved') { continue; }
        if (!originalRows[i]) { continue; }
        if (!originalRows[i][key]) { continue; }
        if (originalRows[i][key].indexOf(toSearch) !== -1) {
          results.push(originalRows[i]);
        }
      }
    }

    let ids = results.map(e => e._id);
    let uniqueIds = [...new Set(ids)];
    uniqueIds.forEach(e => {
      let found = originalRows.find(row => row._id === e);
      finalResults.push(found);
    })

    this.setState(previousState => ({
      rows: rows,
    }), () => {
      this.setState({ rows: finalResults, loading: false, searching: false });
    });
  }

  onChangeSearch(e) {
    this.setState({
      searchVal: e.target.value
    });
     
    setTimeout(()=> { this.onSearch(); }, 500)
  }
 


  getTable(items) {
    return (
      items.length > 0 && !this.state.refresh ? <div>
        <ReactTable
          defaultPageSize={5}
          getTrProps={(state, rowInfo, column) => {
            if(!rowInfo){return {
              style: {
              }
            };}

            return {
              style: {
                background: rowInfo.row.selected  ? "#0069D9" : "",
                color: rowInfo.row.selected  ? "white" : "",
              }
            };
          }}

          data={this.state.rows}
          columns={this.columns} />
      </div> : null

    )

  }

  getList(items) {
    return (
      <div>
        list
        <ul>
          {items.map((e, index) => {
            return <li key={index}>{e.name}</li>
          })}
        </ul></div>

    )
  }


  getNoResultsFound(length) {
    if (length === 0 || !length) {
      return (
        <div>
          <p>No Match found</p>
        </div>

      )
    }
  }

  cancelSearch() {
    this.setState({ refresh: true })
    this.setState(previousState => ({
      rows: originalRows,
    }), () => {
      this.setState({ rows: originalRows, searchVal: '', refresh: false });

    });

  }

 

  getUndo() {
    return (
      this.state.searchVal ? <span

        type="submit" onClick={this.cancelSearch} className="fas fa-times undoItem"></span> : null
    );
  }


 


  render() {

    return (
      <div className="container">
        {/* {this.getList(this.state.rows)} */}



        <header className="jumbotron">


          <Form
            onSubmit={this.onSearch}
            ref={c => {
              this.form = c;
            }}
          >

            <div className="flexed-container">
              <div className="form-group flexDisplayRow">

                <Input
                  type="text"
                  placeholder="Search"
                  className="form-control"
                  name="searchVal"
                  onChange={this.onChangeSearch}
                  value={this.state.searchVal}
                />

                {this.getUndo()}



              </div>
            </div>

            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>



          {this.getNoResultsFound(this.state.rows.length)}
          <div style={{ width: '90%' }}>
            {this.getTable(this.state.rows)}
          </div>
        </header>
      </div>
    );
  }
}
