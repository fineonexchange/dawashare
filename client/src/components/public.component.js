import React, { Component } from "react";
import PublicService from "../services/public.service";
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import EventBus from "../common/EventBus";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Textarea from "react-validation/build/textarea";

import CheckButton from "react-validation/build/button";
import Dialog, {
  DialogContent,
  DialogFooter,
  DialogButton,
} from '@material/react-dialog';


const rows = [];
var originalRows = [];
 
export default class PublicHome extends Component {

  constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
    this.cancelSearch = this.cancelSearch.bind(this);
    this.columns = [
      {
        Header: "Medicine Name",
        accessor: "name",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Phone",
        accessor: "phone",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Area",
        accessor: "area",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },{
        id: 'edit',
        Header: "Actions",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' },
        accessor: '_id',
        // Set the custom cell render here:
        Cell: (cell) => (
          <div>
             {/* <button  onClick={() => this.reportUser(cell)} className="btn-secondary">Report User</button>  */}
             <button onClick={() => this.reportMed(cell)}   className="btn-secondary" style={{'marginLeft':'5px'}}>Report Record</button>
            

          </div>
        )
      }
    ];

    this.reportUser = (e) => {
       this.setState({isOpen1:true, userReportText:'', userId: e.original.userId, medId:e.original._id})
    }

    this.reportMed = (e) => {
      this.setState({isOpen2:true, medReportText:'', userId: e.original.userId, medId:e.original._id})
   }

    this.onChangeSearch = this.onChangeSearch.bind(this);
    
    this.state = {
      isOpen1:false,
      isOpen2:false,
      content: "",
      rows: rows,
      editMode: false,
      originalRows: originalRows,
    };
  }


 

  onSearch = () => {
    let toSearch = this.state.searchVal ? this.state.searchVal.toLowerCase() : null;
    originalRows = this.state.originalRows;
    this.setState({ loading: true, searching: true });
    let results = [];
    let finalResults = [];
    for (var i = 0; i < originalRows.length; i++) {
      for (let key in originalRows[i]) {
        if (key === 'userData' || key === '_id') { continue; }
        if (!originalRows[i]) { continue; }
        if (!originalRows[i][key]) { continue; }
        if (originalRows[i][key].indexOf(toSearch) !== -1) {
          results.push(originalRows[i]);
        }
      }
    }

    let ids = results.map(e => e._id);
    let uniqueIds = [...new Set(ids)];
    uniqueIds.forEach(e => {
      let found = originalRows.find(row => row._id === e);
      finalResults.push(found);
    })

    this.setState(previousState => ({
      rows: rows,
    }), () => {
      this.setState({ rows: finalResults, loading: false, searching: false });
    });
  }

  onSubmitUserReport = async () => {
  let resp = await  PublicService.reportUser(this.state.userId, this.state.medId, this.state.userReportText);
   if(resp.data.message === 'success'){
     this.setState({isOpenReportSent:true});
   }
  }


  onSubmitMedReport = async () => {
    let resp = await PublicService.reportMed(this.state.userId, this.state.medId, this.state.medReportText);
    if(resp.data.message === 'success'){
      this.setState({isOpenReportSent:true});
    }
  }


  onChangeSearch(e) {
    this.setState({
      searchVal: e.target.value
    });

    this.onSearch();
  }
 
  onUserReportChange =(e)=> {
    this.setState({
      userReportText: e.target.value
    });
  }

  onMedReportChange = (e)=> {
    this.setState({
      medReportText: e.target.value
    });
  }

  componentDidMount() {
    PublicService.getAllMedicines().then(resp => {
      let originalRows = [...resp.data.data];
      this.setState({ rows: resp.data.data, originalRows: originalRows, loading: false });
    }, error => {
      this.setState({
        content:
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString()
      });

      if (error.response && error.response.status === 401) {
        EventBus.dispatch("logout");
      }
    }
    )
  }


  getTable(items) {
    return (
      items.length > 0 && !this.state.refresh ? <div>
        <ReactTable
          defaultPageSize={5}
          getTrProps={(state, rowInfo, column) => {
            if (!rowInfo) {
              return {
                style: {
                }
              };
            }

            return {
              style: {
                background: rowInfo.row.selected ? "#0069D9" : "",
                color: rowInfo.row.selected ? "white" : "",
              }
            };
          }}

          data={this.state.rows}
          columns={this.columns} />
      </div> : null

    )

  }

  getList(items) {
    return (
      <div>
        list
        <ul>
          {items.map((e, index) => {
            return <li key={index}>{e.name}</li>
          })}
        </ul></div>

    )
  }


  getNoResultsFound(length) {
    if (length === 0 || !length) {
      return (
        <div>
          <p>No Match found</p>
        </div>

      )
    }
  }


  cancelSearch() {
    this.setState({ refresh: true })
    this.setState(previousState => ({
      rows: originalRows,
    }), () => {
      this.setState({ rows: originalRows, searchVal: '', refresh: false });

    });
  }


  getUndo() {
    return (
      this.state.searchVal ? <span

        type="submit" onClick={this.cancelSearch} className="fas fa-times undoItem"></span> : null
    );
  }

 

  render() {

    return (
      <div className="container">
        {/* {this.getList(this.state.rows)} */}
        <header className="jumbotron">
          <Form
            onSubmit={this.onSearch}
            ref={c => {
              this.form = c;
            }}
          >

            <div className="flexed-container">
              <div className="form-group flexDisplayRow">

                <Input
                  type="text"
                  placeholder="Search"
                  className="form-control"
                  name="searchVal"
                  onChange={this.onChangeSearch}
                  value={this.state.searchVal}
                />

                {this.getUndo()}
              </div>
            </div>

            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>

          <Dialog
        onClose={(action) => this.setState({isOpen1: false, action})}
        open={this.state.isOpen1}>
        <DialogContent>
          <p>Report User? Please leave a comment why.</p>
          <Form
            onSubmit={this.onSubmitUserReport}
            ref={c => {
              this.form = c;
            }}
          >

            <div className="flexed-container">
              <div className="form-group flexDisplayRow">

                <Textarea
                  type="text"
                  placeholder="Comment"
                  className="form-control comment"
                  name="userReportText"
                  onChange={this.onUserReportChange}
                  value={this.state.userReportText}
                />

                </div>
                </div>
                </Form>

        </DialogContent>
        <DialogFooter>
          <DialogButton action='dismiss'>Cancel</DialogButton>
          <DialogButton 
          disabled={!this.state.userReportText}
          onClick={this.onSubmitUserReport} action='discard' isDefault>Submit</DialogButton>
        </DialogFooter>
      </Dialog>

      <Dialog
        onClose={(action) => this.setState({isOpen2: false, action})}
        open={this.state.isOpen2}>
           <DialogContent>
          <p>Report Medicine record? Please leave a comment why.</p>
          <Form
            onSubmit={this.onMedReportChange}
            ref={c => {
              this.form = c;
            }}
          >

            <div className="flexed-container">
              <div className="form-group flexDisplayRow">

                <Textarea
                  type="text"
                  placeholder="Comment"
                  className="form-control comment"
                  name="medReportText"
                  onChange={this.onMedReportChange}
                  value={this.state.medReportText}
                />

                </div>
                </div>
                </Form>

        </DialogContent>
        <DialogFooter>
          <DialogButton action='dismiss'>Cancel</DialogButton>
          <DialogButton 
           disabled={!this.state.medReportText}
          onClick={this.onSubmitMedReport} action='discard' isDefault>Submit</DialogButton>
        </DialogFooter>
      </Dialog>











      <Dialog
        onClose={(action) => this.setState({isOpenReportSent: false, action})}
        open={this.state.isOpenReportSent}>
           <DialogContent>
          Report sent
          </DialogContent>
        <DialogFooter>
          <DialogButton action='dismiss'>Ok</DialogButton>
        </DialogFooter>
      </Dialog>

          {this.getNoResultsFound(this.state.rows.length)}
          <div style={{ width: '90%' }}>
            {this.getTable(this.state.rows)}
          </div>
        </header>
      </div>
    );
  }
}
