import React, { Component } from "react";

import ModService from "../services/mod.service";
import EventBus from "../common/EventBus";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import Input from "react-validation/build/input";
import Select from "react-validation/build/select";
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import Textarea from "react-validation/build/textarea";
import Dialog, {
  DialogContent,
  DialogFooter,
  DialogButton,
} from '@material/react-dialog';


/* const columns = [
  { key: "_id", name: "id", width: "20%", hidden: false, resizable: true },
  { key: "username", name: "User", width: "10%", resizable: true },

  { key: "phone", name: "Phone/Mobile Number", editable: true, width: "40%", resizable: true },
  { key: "area", name: "Area", editable: true, width: "10%", resizable: true },

  { key: "action", name: "Actions", resizable: true, width: "10%" }
]; */



const rows = [

];
const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};
var originalRows = [];



export default class BoarMod extends Component {

  constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.cancelSearch = this.cancelSearch.bind(this);
    this.columns = [
      {
        Header: "User",
        accessor: "username",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Med Name",
        accessor: "name",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Phone",
        accessor: "phone",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },
      {
        Header: "Area",
        accessor: "area",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },

      {
        Header: "Status",
        accessor: "status",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' }
      },

      {
        Header: "Selected",
        accessor: "selected",
        show: false
      },
      {
        id: 'edit',
        Header: "Actions",
        headerStyle: { background: `#343A40`, color: 'white', 'borderRight': '1px solid white' },
        accessor: '_id',
        width: 200,
        // Set the custom cell render here:
        Cell: (cell) => (
          <div>
            <span
              onClick={() => this.handleEdit(cell)}
              className="fas fa-edit edit" />

            <span
              onClick={() => this.handleDelete(cell)}
              className="fas fa-trash-alt delete" />
            {
              cell.original.status === 'pending' && <button  onClick={() => this.acceptMed(cell)} className="btn-secondary" style={{ 'marginLeft': '5px' }}>Accept</button>
            }
            {
              cell.original.status === 'pending' && <button  onClick={() => this.rejectMed(cell)} className="btn-secondary" style={{ 'marginLeft': '5px' }}>Reject</button>
            }
          </div>
        )
      },

    ];

    this.onChangeMedicine = this.onChangeMedicine.bind(this);
    this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this);
    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.state = {
      isOpen1: false,
      content: "",
      rows: rows,
      editMode: 'No',
      originalRows: originalRows,
    };

    this.acceptMed = (row) => {
      const rows = [...this.state.rows];
      const rowId = row.value;
      let index = rows.findIndex(e => e._id === rowId);
      rows[index].status = 'accepted';
      ModService.acceptMedicine(row.original._id);
  
      this.setState(previousState => ({
        rows: rows,
      }), () => {
        this.setState({ rows: rows });
      });

    }


    this.rejectMed = (e) => {
      this.setState({ isOpen: true, medRejectText: '', userId: e.original.userId, medId: e.original._id })
    }
  }

  


  handleDelete = (row) => {
    const rows = [...this.state.rows];
    const rowId = row.value;
    let index = rows.findIndex(e => e._id === rowId);
    rows.splice(index, 1);
    ModService.deleteMedicine(row.original._id);


    this.setState(previousState => ({
      rows: rows,
    }), () => {
      this.setState({ rows: rows });
    });
  }

  handleEdit = (cell) => {
    let rowId = cell.value;
    let row = this.state.rows.find(e => e._id === rowId);
    let index = this.state.rows.findIndex(e => e._id === rowId);

    let rows = [...this.state.rows];
    rows.forEach(e => e.selected = false);
    rows[index] = { ...rows[index], selected: true };
    this.setState({ rows });


    if (row) {
      this.setState(previousState => ({
        editMode: 'Yes',
      }), () => {
        this.setState({ editMode: 'Yes', name: row.name, phone: row.phone, area: row.area, id: row._id });
      });
    }

  }





  onSubmitRejection = async () => {
    let resp = await ModService.rejectMedicine(this.state.medId, this.state.medRejectText);
    if (resp.data.message === 'success') {
      this.setState({ isOpenReportSent: true });
        window.location.reload();
    }
  }

  onSearch = () => {

    let toSearch = this.state.searchVal ? this.state.searchVal.toLowerCase() : null;
    originalRows = this.state.originalRows;
    this.setState({ loading: true, searching: true });
    let results = [];
    let finalResults = [];
    for (var i = 0; i < originalRows.length; i++) {
      for (let key in originalRows[i]) {
        if (key === 'userData' || key === '_id') { continue; }
        if (!originalRows[i]) { continue; }
        if (!originalRows[i][key]) { continue; }
        if (originalRows[i][key].indexOf(toSearch) !== -1) {
          results.push(originalRows[i]);
        }
      }
    }

    let ids = results.map(e => e._id);
    let uniqueIds = [...new Set(ids)];
    uniqueIds.forEach(e => {
      let found = originalRows.find(row => row._id === e);
      finalResults.push(found);
    })

    this.setState(previousState => ({
      rows: rows,
    }), () => {
      this.setState({ rows: finalResults, loading: false, searching: false });
    });
  }


  onChangeSearch(e) {
    this.setState({
      searchVal: e.target.value
    });

    this.onSearch();
  }

  onChangeMedicine(e) {
    this.setState({
      name: e.target.value
    });
  }

  onChangePhoneNumber(e) {
    this.setState({
      phone: e.target.value
    });
  }

  onChangeLocation(e) {
    this.setState({
      area: e.target.value
    });
  }

  onMedRejectChange = (e) => {
    this.setState({
      medRejectText: e.target.value
    });
  }


  getRowCount() {
    let count = this.state.rows.length;
    if (this.state.refresh && count > 0) {
      count--; // hack for update data-grid
      this.setState({
        refresh: false
      });
    }

    return count;
  }

  refresh() {
    this.setState({
      refresh: true
    });
  }

  handleCancel = () => {
    let rows = [...this.state.rows];
    rows.forEach(e => e.selected = false);
    this.setState({ editMode: false, rows })
  }

  handleUpdate = (e) => {
    e.preventDefault();
    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      let formData = { name: this.state.name, phone: this.state.phone, area: this.state.area };
      const { rows } = this.state;
      ModService.updateMedicine(this.state.id, formData).then(resp => {
        let updatedRows = [...rows];
        let findIndex = this.state.rows.findIndex(e => e._id === this.state.id);
        Object.keys(formData).forEach(key => {
          updatedRows[findIndex][key] = formData[key];
        })
        this.setState({ rows: updatedRows, loading: false, editMode: false });
        this.handleCancel();
      })

    } else {
      this.setState({
        loading: false
      });
    }
  }

  componentDidMount() {
    ModService.getAllMedicines().then(resp => {
      let originalRows = [...resp.data.data];
      this.setState({ rows: resp.data.data, originalRows: originalRows, loading: false });
    }, error => {
      this.setState({
        content:
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString()
      });

      if (error.response && error.response.status === 401) {
        EventBus.dispatch("logout");
      }
    }
    )
  }


  getTable(items) {
    return (
      items.length > 0 && !this.state.refresh ? <div>
        <ReactTable
          defaultPageSize={5}
          getTrProps={(state, rowInfo, column) => {
            if (!rowInfo) {
              return {
                style: {
                }
              };
            }

            return {
              style: {
                background: rowInfo.row.selected ? "#0069D9" : "",
                color: rowInfo.row.selected ? "white" : "",
              }
            };
          }}

          data={this.state.rows}
          columns={this.columns} />
      </div> : null

    )

  }

  getList(items) {
    return (
      <div>
        list
        <ul>
          {items.map((e, index) => {
            return <li key={index}>{e.name}</li>
          })}
        </ul></div>

    )
  }


  getNoResultsFound(length) {
    if (length === 0 || !length) {
      return (
        <div>
          <p>No Match found</p>
        </div>

      )
    }
  }

  cancelSearch() {
    this.setState({ refresh: true })
    this.setState(previousState => ({
      rows: originalRows,
    }), () => {
      this.setState({ rows: originalRows, searchVal: '', refresh: false });

    });

  }



  getUndo() {
    return (
      this.state.searchVal ? <span

        type="submit" onClick={this.cancelSearch} className="fas fa-times undoItem"></span> : null
    );
  }



  getEditForm() {
    return (
      this.state.editMode === 'Yes' ?
        <div>
          <Form
            onSubmit={this.handleUpdate}
            ref={c => {
              this.form = c;
            }}
          >
            <div className="flexed-container">
              <div className="form-group flex-child">
                <label htmlFor="name">Medicine Name</label>
                <Input
                  type="text"
                  className="form-control"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChangeMedicine}
                  validations={[required]}
                />
              </div>

              <div className="form-group flex-child">
                <label htmlFor="phone">Phone/Mobile Number</label>
                <Input
                  type="phone"
                  className="form-control"
                  name="phone"
                  value={this.state.phone}
                  onChange={this.onChangePhoneNumber}
                  validations={[required]}
                />
              </div>


              <div className="form-group flex-child">
                <label htmlFor="area">Area</label>
                <Select
                  type="area"
                  className="form-control"
                  name="area"
                  value={this.state.area}
                  onChange={this.onChangeLocation}
                  validations={[required]} >
                  <option value="All" >- Select Area -</option>
                  <option value="Aakkar">Aakkar</option>
                  <option value="Aaley">Aaley</option>
                  <option value="Baabda">Baabda</option>
                  <option value="Baalbek">Baalbek</option>
                  <option value="Batroun">Batroun</option>
                  <option value="Bcharreh">Bcharreh</option>
                  <option value="Bent Jbeil">Bent Jbeil</option>
                  <option value="Beqaa Ouest">Beqaa Ouest</option>
                  <option value="Beyrouth">Beyrouth</option>
                  <option value="Chouf">Chouf</option>
                  <option value="Hasbaiya">Hasbaiya</option>
                  <option value="Hermel">Hermel</option>
                  <option value="Jbeil">Jbeil</option>
                  <option value="Jezzine">Jezzine</option>
                  <option value="Kesrouane">Kesrouane</option>
                  <option value="Koura">Koura</option>
                  <option value="Marjaayoun">Marjaayoun</option>
                  <option value="Matn">Matn</option>
                  <option value="Minieh-Danniyeh">Minieh-Danniyeh</option>
                  <option value="Nabatiyeh">Nabatiyeh</option>
                  <option value="Rachaiya">Rachaiya</option>
                  <option value="Saida">Saida</option>
                  <option value="Sour">Sour</option>
                  <option value="Tripoli">Tripoli</option>
                  <option value="Zahleh">Zahleh</option>
                  <option value="Zgharta">Zgharta</option>
                </Select>



              </div>



              <div className="form-group flexed-container flex-child  addBtn">

                <button
                  type="submit"
                  className="btn btn-primary updateBtn btn-block"
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Update</span>
                </button>
              </div>

              <div className="form-group flexed-container flex-child  addBtn">
                <button
                  onClick={this.handleCancel}
                  className="btn btn-primary btn-block"
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Cancel</span>
                </button>
              </div>
            </div>





            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
          <br></br>

        </div>

        : null
    );

  }


  render() {

    return (
      <div className="container">
        {/* {this.getList(this.state.rows)} */}



        <header className="jumbotron">


          <Form
            onSubmit={this.onSearch}
            ref={c => {
              this.form = c;
            }}
          >

            <div className="flexed-container">
              <div className="form-group flexDisplayRow">

                <Input
                  type="text"
                  placeholder="Search"
                  className="form-control"
                  name="searchVal"
                  onChange={this.onChangeSearch}
                  value={this.state.searchVal}
                />

                {this.getUndo()}



              </div>
            </div>

            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>


          {this.getEditForm()}

          {this.getNoResultsFound(this.state.rows.length)}
          <div style={{ width: '90%' }}>
            {this.getTable(this.state.rows)}
          </div>
        </header>




        <Dialog
          onClose={(action) => this.setState({ isOpen: false, action })}
          open={this.state.isOpen}>
          <DialogContent>
            <p>Reason of Rejection</p>
            <Form
              onSubmit={this.onMedRejectChange}
              ref={c => {
                this.form = c;
              }}
            >

              <div className="flexed-container">
                <div className="form-group flexDisplayRow">

                  <Textarea
                    type="text"
                    placeholder="Comment"
                    className="form-control comment"
                    name="medRejectText"
                    onChange={this.onMedRejectChange}
                    value={this.state.medRejectText}
                  />

                </div>
              </div>
            </Form>

          </DialogContent>
          <DialogFooter>
            <DialogButton action='dismiss'>Cancel</DialogButton>
            <DialogButton
              disabled={!this.state.medRejectText}
              onClick={this.onSubmitRejection} action='discard' isDefault>Submit</DialogButton>
          </DialogFooter>
        </Dialog>

        <Dialog
          onClose={(action) => this.setState({ isOpenReportSent: false, action })}
          open={this.state.isOpenReportSent}>
          <DialogContent>
            Rejection sent
          </DialogContent>
          <DialogFooter>
            <DialogButton action='dismiss'>Ok</DialogButton>
          </DialogFooter>
        </Dialog>


      </div>





    );
  }
}
