import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/mod/';

class ModService {
 
  getAllMedicines() {
    return axios.get(API_URL + 'medicines', { headers: authHeader() });
  }

  deleteMedicine(id) {
    return axios.delete(API_URL + `delete/${id}`, { headers: authHeader()  });
  }

  getReports(){
    return axios.get(API_URL + 'reports', { headers: authHeader() });
  }

  updateReport(id, isResolved){
    return axios.post(API_URL + 'updateReport', {id, isResolved}, { headers: authHeader() });
  }

  assignToMe(id, isAssigned){
    return axios.post(API_URL + 'assignToMe', {id, isAssigned}, { headers: authHeader() });
  }
  
  updateMedicine(id, formData) {
    return axios.post(API_URL + `updateMedicine`,  {id, formData}, { headers: authHeader()  });
  }

  acceptMedicine(id) {
    return axios.post(API_URL + `acceptMedicine`,  {id}, { headers: authHeader()  });
  }

  rejectMedicine(id, formData) {
    return axios.post(API_URL + `rejectMedicine`,  {id, formData}, { headers: authHeader()  });
  }

  getUsers(){
    return axios.get(API_URL + 'getUsers', { headers: authHeader() });
  }
  banUser(id, isBanned){
    return axios.post(API_URL + 'banUser', {id, isBanned}, { headers: authHeader() });
  }

}

export default new ModService();
