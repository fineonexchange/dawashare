import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/admin/';

class AdminService {
 
  getAllUsers() {
    return axios.get(API_URL + 'getAllUsers', { headers: authHeader() });
  }

  updateUserRole(id, formData) {
    return axios.post(API_URL + `updateUserRole`,  {id, formData}, { headers: authHeader()  });
  }

  
  banUser(id, isBanned){
    return axios.post(API_URL + 'banUser', {id, isBanned}, { headers: authHeader() });
  }

}

export default new AdminService();
