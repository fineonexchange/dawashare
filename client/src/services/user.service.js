import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/user/';

class UserService {

  getUserBoard() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

  getMyMedicines() {
    return axios.get(API_URL + 'medicines', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }

  addMedicineAsUser(formData) {
    return axios.post(API_URL + 'add', {formData}, { headers: authHeader()  });
  }

  updateMedicine(id, formData) {
    return axios.post(API_URL + 'update', {formData, id}, { headers: authHeader()  });
  }

  deleteMedicine(id) {
    return axios.delete(API_URL + `delete/${id}`, { headers: authHeader()  });
  }


  
}

export default new UserService();
