import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/public/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getAllMedicines() {
    console.log("HERE")
    return axios.get(API_URL + 'medicines', { headers: authHeader() });
  }


  reportUser(userId,medId,text){
    return axios.post(API_URL + 'reportUser', {userId,medId,text}, { headers: authHeader() });
  }
  reportMed(userId,medId,text){
    return axios.post(API_URL + 'reportMed', {userId,medId,text}, { headers: authHeader() });
  }
  
}

export default new UserService();
