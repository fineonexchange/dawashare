import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-datasheet/lib/react-datasheet.css'
import "./App.scss";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/public.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import BoardAdmin from "./components/board-admin.component";
import ManageReports from "./components/manage-reports";
import BanUsers from "./components/ban-users";

// import AuthVerify from "./common/auth-verify";
import EventBus from "./common/EventBus";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      showUserBoard:false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
    let isResetPasswordMode = user.isResetPasswordMode? true: false;

      this.setState({
        currentUser: user,
        path:window.location.pathname,
        showUserBoard: user.roles.includes("ROLE_USER") && ! isResetPasswordMode,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR") && ! isResetPasswordMode,
        showAdminBoard: user.roles.includes("ROLE_ADMIN") && !isResetPasswordMode,
        showResetPassword: isResetPasswordMode
      });
    }
    
    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    AuthService.logout();
    this.setState({
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    });
  }

  setPath() {
    const pathname = window.location.pathname;
    this.setState({path: pathname});
  }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard, showResetPassword, showUserBoard } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="navbar-brand">
            DawaShare
          </Link>
          <div className="navbar-nav mr-auto">
            <li     onClick={() => this.setPath()}
            className="nav-item">
             <Link to={"/home"} className={this.state.path === '/home' ? 'nav-link-selected nav-link' :'nav-link'}>
                Search Medicine
              </Link>
            </li>


            {showModeratorBoard && (
              <li 
              onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/mod"} className={this.state.path === '/mod' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Manage Medicines
                </Link>
              </li>
            )}

            {showModeratorBoard && (
              <li 
              onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/ban"} className={this.state.path === '/ban' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Ban Users
                </Link>
              </li>
            )}

            {showResetPassword && (
              <li 
              onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/reset"} className={this.state.path === '/reset' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Reset Password
                </Link>
              </li>
            )}
            

            {showModeratorBoard && (
              <li 
               onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/reports"} className={this.state.path === '/reports' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Manage Reports
                </Link>
              </li>
            )}


            {showAdminBoard && (
              <li 
               onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/admin"} className={this.state.path === '/admin' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Admin Board
                </Link>
              </li>
            )}

            {showUserBoard && (
              <li 
               onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/user"} className={this.state.path === '/user' ? 'nav-link-selected nav-link' :'nav-link'}>
                  Input Medicines
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li 
               onClick={() => this.setPath()}
              className="nav-item">
                <Link to={"/profile"} className={this.state.path === '/profile' ? 'nav-link-selected nav-link' :'nav-link'}>
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Logout
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user" component={BoardUser} />
            <Route path="/mod" component={BoardModerator} />
            <Route path="/admin" component={BoardAdmin} />
            <Route path="/reports" component={ManageReports} />
            <Route path="/ban" component={BanUsers} />
          </Switch>
        </div>

        { /*<AuthVerify logOut={this.logOut}/> */ }
      </div>
    );
  }
}

export default App;
